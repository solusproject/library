const fetch = require('node-fetch');
const server = 'localhost'

exports.explicitCheck = function(message) {
	return new Promise(function(resolve, reject) {
	fetch("https://gitlab.com/api/v4/projects/35050367/repository/files/dirty.txt/raw?ref=main").then(response => response.text()).then(data => {
		const badWords = data.split("\n")
		for (badWord of badWords) {
			if (message.toLowerCase().includes(badWord)){
				resolve({score:3, confidence:1, points:3})
			}
		}
	});
});
}